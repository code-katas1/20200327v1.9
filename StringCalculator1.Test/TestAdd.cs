﻿using NUnit.Framework;
using System;

namespace StringCalculator1.Test
{
    [TestFixture]
    public class TestAdd
    {
        Calculator _calculator = null;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("1,2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleNumber_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("1,2,3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewLineAsDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("1,2\n3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("//;\n1;2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumbers_WHEN_Adding_THEN_ThrowException()
        {
            var expected = "Negative numbers not allowed -5,-6";
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;-5;-6;4"));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumbersMoreThanAThousand_WHEN_Adding_THEN_IgnoreThem()
        {
            var expected = 5;
            var actual = _calculator.Add("//$\n3$2$1001");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("//***\n1***2***3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleDelimiters_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 12;
            var actual = _calculator.Add("//[*][#]\n1*2*3#4#2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleDelimitersWithMultipleCharacters_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 12;
            var actual = _calculator.Add("//[***][###]\n1***2***3###4###2");

            Assert.AreEqual(expected, actual);
        }
    }
}
