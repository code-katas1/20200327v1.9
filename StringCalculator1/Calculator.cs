﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator1
{
    public class Calculator
    {
        private string _customDelimiterId = "//";
        private string _newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string[] delimiters = GetDelimiters(numbers);
            string numbersSection = GetNumbersSection(numbers);
            List<int> numbersArray = GetNumbers(delimiters, numbersSection);

            ValidateNumbers(numbersArray);

            return GetSum(numbersArray);
        }

        private string[] GetDelimiters(string numbers)
        {
            var multipleCustomDelimiterId = $"{_customDelimiterId}[";
            var multipleCustomDelimiterSeperator = $"]{_newline}";
            var multipleCustomDelimiterSpliter = "][";

            if (numbers.StartsWith(multipleCustomDelimiterId))
            {
                string delimiter = numbers.Substring(numbers.IndexOf(multipleCustomDelimiterId) + multipleCustomDelimiterId.Length, numbers.IndexOf(multipleCustomDelimiterSeperator) - (multipleCustomDelimiterSeperator.Length + 1));
                return delimiter.Split(new[] { multipleCustomDelimiterSpliter }, StringSplitOptions.RemoveEmptyEntries);
                        }
            else if (numbers.StartsWith(_customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(_customDelimiterId) + _customDelimiterId.Length, numbers.IndexOf(_newline) - (_newline.Length + 1)) };
            }

            return new[] { ",", _newline };
        }

        private string GetNumbersSection(string numbers)
        {
            if(numbers.StartsWith("//"))
            {
                return numbers.Substring(numbers.IndexOf("\n") + 1);
            }

            return numbers;
        }

        private List<int> GetNumbers(string[] delimiters, string numbersSection)
        {
            string[] stringNumbers = numbersSection.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersArray = new List<int>();

            foreach (var num in stringNumbers)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersArray.Add(number);
                }
            }

            return numbersArray;
        }

        private void ValidateNumbers(List<int> numbersArray)
        {
            List<string> negativeNumber = new List<string>();

            foreach (var number in numbersArray)
            {
                if (number < 0)
                {
                    negativeNumber.Add(number.ToString());
                }
            }

            if (negativeNumber.Count != 0)
            {
                throw new Exception($"Negative numbers not allowed {string.Join(",", negativeNumber)}");
            }
        }

        private int GetSum(List<int> numbersArray)
        {
            int sum = 0;

            foreach (var number in numbersArray)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }
    }
}
